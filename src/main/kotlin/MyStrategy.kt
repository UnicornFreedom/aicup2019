@file:Suppress("ConstantConditionIf")

import model.*
import kotlin.math.*
import model.Unit as GUnit

class MyStrategy {
    fun getAction(me: GUnit, game: Game, debug: Debug): UnitAction {
        // init necessary data && provide logic with the current state
        init(me, game, debug)
        clearMaps()
        calculateDangerMap()
        calculateObstaclesMap()
        calculateCostMap()
        calculateItemsMap()

        if (RENDER_DEBUG && RENDER_DEBUG_MAPS) {
            renderMaps()
            renderFiringPositions()
        }

        val nearestEnemy = nearestEnemy(me, game)
        val nearestWeapon = nearestItem(game, ItemType.WEAPON)

        // prepare the action
        val action = UnitAction()

        // what we are doing right now
        val missionDirective = when {
            nearestEnemy != null && isItSafeToRush(nearestEnemy) ->
                if (inClinchZone(nearestEnemy)) MissionDirective.CLINCH
                else MissionDirective.GOING_TO_CLINCH
            (me.health < UNIT_MAX_HEALTH * 0.8) && anyItem(game, ItemType.HEALTH) != null -> MissionDirective.HEAL
            me.weapon == null -> MissionDirective.FIND_A_WEAPON
            isReloading(me) -> MissionDirective.HIDE
            else -> MissionDirective.FIGHT
        }

        if (RENDER_DEBUG) {
            renderText(MAP_WIDTH.toDouble(), MAP_HEIGHT.toDouble(), missionDirective.toString(), COLOR_ORANGE, TextAlignment.RIGHT, 20f)
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // AIM
        // The bot is always aiming at the enemy, so when he will be able to actually shoot - the aim will be correct,
        // and the sudden change of angle will not increase the spread.

        val aimPosition = nearestEnemy?.position ?: me.position
        action.aim = Vec2Double(aimPosition.x - me.position.x, aimPosition.y - me.position.y)

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // SHOOT
        // Does the boot need to shoot right now?

        if (me.weapon != null && nearestEnemy != null) {
            val spread = me.weapon!!.spread
            val bulletSize = me.weapon!!.params.bullet.size

            val meX = me.position.x
            val meY = me.position.y + UNIT_HEIGHT / 2
            val aimX = aimPosition.x
            val aimY = aimPosition.y + UNIT_HEIGHT / 2

            val mainHit = bulletTracerSqr(meX, meY, aimX, aimY, bulletSize)
            val mainVisual = if (hasRocketLauncher(me))
                distanceSqr(mainHit?.collisionX ?: aimX, mainHit?.collisionY
                        ?: aimY, aimX, aimY) < ROCKET_EXP_RADIUS_SQR &&
                        (missionDirective == MissionDirective.CLINCH || missionDirective == MissionDirective.GOING_TO_CLINCH ||
                                distanceSqr(mainHit?.collisionX ?: aimX, mainHit?.collisionY
                                        ?: aimY, meX, meY) > ROCKET_EXP_RADIUS_SQR)
            else
                mainHit == null || obstaclesMap[mainHit.tileX][mainHit.tileY] == Obstacle.ENEMY
            if (RENDER_DEBUG && RENDER_DEBUG_AIM)
                renderLine(meX, meY, aimX, aimY, bulletSize.toFloat(), if (mainVisual) COLOR_WEAPON_TRACER_FREE else COLOR_WEAPON_TRACER_BLOCKED)

            if (hasRocketLauncher(me) && missionDirective != MissionDirective.CLINCH) {
                val leftX = cos(spread) * (aimX - meX) - sin(spread) * (aimY - meY) + meX
                val leftY = sin(spread) * (aimX - meX) + cos(spread) * (aimY - meY) + meY
                val leftHit = bulletTracerSqr(meX, meY, leftX, leftY, bulletSize)
                val leftVisual = (leftHit?.distance ?: Double.MAX_VALUE) > ROCKET_EXP_RADIUS_SQR ||
                        (missionDirective == MissionDirective.GOING_TO_CLINCH && distanceSqr(leftHit?.collisionX
                                ?: aimX, leftHit?.collisionY ?: aimY, aimX, aimY) < ROCKET_EXP_RADIUS_SQR)
                if (RENDER_DEBUG && RENDER_DEBUG_AIM)
                    renderLine(meX, meY, leftX, leftY, 0.1f, if (leftVisual) COLOR_WEAPON_TRACER_FREE else COLOR_WEAPON_TRACER_BLOCKED)

                val rightX = cos(-spread) * (aimX - meX) - sin(-spread) * (aimY - meY) + meX
                val rightY = sin(-spread) * (aimX - meX) + cos(-spread) * (aimY - meY) + meY
                val rightHit = bulletTracerSqr(meX, meY, rightX, rightY, bulletSize)
                val rightVisual = (rightHit?.distance ?: Double.MAX_VALUE) > ROCKET_EXP_RADIUS_SQR ||
                        (missionDirective == MissionDirective.GOING_TO_CLINCH && distanceSqr(rightHit?.collisionX
                                ?: aimX, rightHit?.collisionY ?: aimY, aimX, aimY) < ROCKET_EXP_RADIUS_SQR)
                if (RENDER_DEBUG && RENDER_DEBUG_AIM)
                    renderLine(meX, meY, rightX, rightY, 0.1f, if (rightVisual) COLOR_WEAPON_TRACER_FREE else COLOR_WEAPON_TRACER_BLOCKED)

                action.shoot = mainVisual && leftVisual && rightVisual
            } else {
                action.shoot = mainVisual
            }
        } else {
            action.shoot = false
        }

        if (RENDER_DEBUG && RENDER_DEBUG_LOGS) debug.draw(CustomData.Log("Magazine: ${me.weapon?.magazine} / Timer: ${me.weapon?.fireTimer}"))

        if (RENDER_DEBUG) {
            renderText(MAP_WIDTH.toDouble(), MAP_HEIGHT.toDouble() - 2, me.weapon?.fireTimer.toString(), COLOR_WHITE, TextAlignment.RIGHT, 20f)
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // MOVEMENT
        // Calculate horizontal velocity.

        val targetPosition = when (missionDirective) {
            MissionDirective.CLINCH, MissionDirective.GOING_TO_CLINCH -> nearestEnemy?.position
            MissionDirective.HEAL -> nearestItem(game, ItemType.HEALTH)?.position
            MissionDirective.FIND_A_WEAPON -> nearestWeapon?.position
            // FIGHT and HIDE
            else -> {
                val aimX = aimPosition.x
                val aimY = aimPosition.y + UNIT_HEIGHT / 2
                val point = nearestFiringPoint(aimX, aimY + UNIT_HEIGHT / 2, missionDirective == MissionDirective.HIDE,
                        if (hasRocketLauncher(nearestEnemy) || hasRocketLauncher(me)) ROCKET_EXP_RADIUS else 0.0)
                if (point != null) Vec2Double(point.x.toDouble() + 0.5, point.y.toDouble() + 0.1)
                else null
            }
        } ?: me.position

        if (RENDER_DEBUG) {
            renderLine(targetPosition.x - 0.5, targetPosition.y + 0.5, targetPosition.x + 0.5, targetPosition.y - 0.5, 0.1f, COLOR_CYAN)
        }

        val nearestAvailablePoint = nearestNotBlockedForPassageTile(targetPosition.x.toInt(), targetPosition.y.toInt())
        if (RENDER_DEBUG && RENDER_DEBUG_PATH) renderRect(nearestAvailablePoint?.x ?: 0, nearestAvailablePoint?.y
                ?: 0, COLOR_WHITE)
        val pathResult = if (nearestAvailablePoint != null) calculatePath(nearestAvailablePoint.x, nearestAvailablePoint.y) else null
        val nextPoint = pathResult?.path?.minBy { distanceSqr(me.position.x, me.position.y, it.x + 0.5, it.y.toDouble()) }
        val targetX = if (nextPoint != null) nextPoint.x + 0.5 else targetPosition.x
        val deltaX = targetX - me.position.x

        action.velocity = when {
            abs(deltaX) < UNIT_MAX_HOR_SPEED -> deltaX * TICKS_PER_SECOND
            deltaX > 0 -> UNIT_MAX_HOR_SPEED
            deltaX < 0 -> -UNIT_MAX_HOR_SPEED
            else -> 0.0
        }

        if (RENDER_DEBUG && nextPoint == null)
            debug.draw(CustomData.PlacedText("NO PATH", Vec2Float(MAP_WIDTH.toFloat(), MAP_HEIGHT.toFloat() - 1), TextAlignment.RIGHT, 20f, COLOR_RED))

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // JUMP
        // Sould the bot jump?

        if (nextPoint != null) {
            if (isTile(me.position.x.toInt(), me.position.y.toInt(), Tile.PLATFORM) && nextPoint.y > me.position.y)
                optimizePlatform = true
            action.jump =
                    if (optimizePlatform && isTile(me.position.x.toInt(), me.position.y.toInt(), Tile.EMPTY)) {
                        optimizePlatform = false; false
                    } else nextPoint.y > me.position.y
            action.jumpDown = (me.position.y - nextPoint.y) > 0.4
        } else {
            action.jump = (targetPosition.y - me.position.y) > 0.4
            action.jumpDown = (me.position.y - targetPosition.y) > 0.4
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // SWAP WEAPON
        // Swap the weapon?

        val nearestWeaponItem = (nearestWeapon?.item as Item.Weapon?)
        action.swapWeapon = missionDirective != MissionDirective.CLINCH && missionDirective != MissionDirective.GOING_TO_CLINCH &&
                shouldISwap(me.weapon, nearestWeaponItem?.weaponType)

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // PLANT MINES
        // Some mines?

        action.plantMine = missionDirective == MissionDirective.FIGHT

        if (RENDER_DEBUG && RENDER_DEBUG_LOGS) {
            debug.draw(CustomData.Log("Can jump: ${me.jumpState.canJump}"))
            debug.draw(CustomData.Log("Can cancel: ${me.jumpState.canCancel}"))
            debug.draw(CustomData.Log("Jump time: ${me.jumpState.maxTime} / Jump height: ${me.jumpState.maxTime * me.jumpState.speed}"))
        }

        // finalize
        return action
    }

    @Suppress("ConvertTwoComparisonsToRangeCheck", "SameParameterValue")
    companion object {
        // additional classes
        private enum class ItemType {
            HEALTH, WEAPON, MINE, ANYTHING
        }

        private enum class MissionDirective {
            FIND_A_WEAPON,   // grab nearest weapon
            GOING_TO_CLINCH, // run to the enemy
            CLINCH,          // block the enemy movement, maximizing the damage
            FIGHT,           // fight the enemy from the distance
            HIDE,            // minimize the enemy chances to deal any damage
            HEAL,            // run to the nearest healing pack
        }

        private enum class Direction {
            UP, UP_RIGHT, RIGHT, DOWN_RIGHT, DOWN, DOWN_LEFT, LEFT, UP_LEFT, NO_DIRECTION
        }

        private data class CostPoint(
                var x: Int, var y: Int, var z: Int,
                var direction: Direction,
                var fromZ: Int,
                var cost: Double,
                var canJump: Boolean, var canCancel: Boolean,
                var jumpHeight: Double,
                var calculated: Boolean)

        private data class PathResult(@Suppress("unused") val success: Boolean, val path: MutableList<Point>?)

        private enum class Obstacle {
            NOTHING, ENEMY, PHANTOM_WALL, MINE, ME
        }

        private data class Hit(val distance: Double, val collisionX: Double, val collisionY: Double, val tileX: Int, val tileY: Int)
        private data class Point(val x: Int, val y: Int)

        // constants & coefficients
        private const val RENDER_DEBUG = true
        private const val RENDER_DEBUG_LOGS = false
        private const val RENDER_DEBUG_MAPS = true
        private const val RENDER_DEBUG_MAPS_COST = true
        private const val RENDER_DEBUG_PATH = true
        private const val RENDER_DEBUG_AIM = true

        private val COLOR_WEAPON_TRACER_FREE = ColorFloat(0f, 1f, 0f, 0.2f)
        private val COLOR_WEAPON_TRACER_BLOCKED = ColorFloat(1f, 0f, 0f, 0.2f)
        private val COLOR_ORANGE = ColorFloat(1f, 0.5f, 0f, 1f)
        private val COLOR_CYAN = ColorFloat(0f, 1f, 1f, 0.3f)
        private val COLOR_MAGENTA = ColorFloat(1f, 0f, 1f, 0.3f)
        private val COLOR_RED = ColorFloat(1f, 0f, 0f, 1f)
        private val COLOR_GRAY = ColorFloat(1f, 1f, 1f, 0.3f)
        private val COLOR_WHITE = ColorFloat(1f, 1f, 1f, 1f)

        private const val USEFUL_ITEM_DIAMETER = 6f
        private const val USEFUL_ITEM_HEALTH = 2f
        private const val USEFUL_ITEM_MINE = 1f

        private const val CIRCULAR_SEARCH_DIAMETER = 10


        // constants that need init
        private var inited = false

        private lateinit var debug: Debug
        private lateinit var me: GUnit
        private lateinit var game: Game

        private var MY_PLAYER_ID: Int = 0
        private var UNIT_WIDTH: Double = 0.0
        private var UNIT_HEIGHT: Double = 0.0
        private var UNIT_MAX_HEALTH: Int = 0
        private var UNIT_MAX_HOR_SPEED: Double = 0.0
        private var UNIT_FALL_SPEED: Double = 0.0
        private var UNIT_JUMP_SPEED: Double = 0.0
        private var UNIT_JUMP_TIME: Double = 0.0
        private var UNIT_JUMP_HEIGHT: Double = 0.0
        private var UNIT_PAD_JUMP_SPEED: Double = 0.0
        private var UNIT_PAD_JUMP_TIME: Double = 0.0
        private var UNIT_PAD_JUMP_HEIGHT: Double = 0.0
        private var ROCKET_EXP_RADIUS: Double = 0.0
        private var ROCKET_EXP_RADIUS_SQR: Double = 0.0
        private var MAP_WIDTH: Int = 0
        private var MAP_HEIGHT: Int = 0
        private var TICKS_PER_SECOND: Double = 60.0

        // path cost constants
        private var COST_HOR: Double = 0.0
        private var COST_JUMP_VER: Double = 0.0
        private var COST_JUMP_VER_PAD: Double = 0.0
        private var COST_JUMP_DIAG: Double = 0.0
        private var COST_JUMP_DIAG_PAD: Double = 0.0
        private var COST_FALL_VER: Double = 0.0
        private var COST_FALL_DIAG: Double = 0.0

        // persistent data
        private var preferredWeaponType = hashMapOf(
                WeaponType.PISTOL to 3.0, // default values, that will be recalculated
                WeaponType.ROCKET_LAUNCHER to 1.0,
                WeaponType.ASSAULT_RIFLE to 2.0
        )
        private var optimizePlatform: Boolean = false

        // maps & positions
        private lateinit var dangerMap: Array<Array<Int>>         // possible damage estimate, used by pathfinder
        private lateinit var obstaclesMap: Array<Array<Obstacle>> // everything that blocks the way temporarily
        private lateinit var itemsMap: Array<Array<Float>>        // useful item proximity estimate (health packs, mines)
        private lateinit var costMap: Array<Array<Array<CostPoint>>>  // z, x, y

        private lateinit var FIRING_POSITIONS: List<Point>


        private fun init(me: GUnit, game: Game, debug: Debug) {
            this.me = me
            this.game = game
            this.debug = debug
            if (!inited) {
                // init constants
                MY_PLAYER_ID = me.playerId
                UNIT_WIDTH = game.properties.unitSize.y
                UNIT_HEIGHT = game.properties.unitSize.y
                UNIT_MAX_HEALTH = game.properties.unitMaxHealth
                UNIT_MAX_HOR_SPEED = game.properties.unitMaxHorizontalSpeed
                ROCKET_EXP_RADIUS = game.properties.weaponParams[WeaponType.ROCKET_LAUNCHER]?.explosion?.radius ?: 2.0
                ROCKET_EXP_RADIUS_SQR = ROCKET_EXP_RADIUS.pow(2)
                MAP_WIDTH = game.level.tiles.size
                MAP_HEIGHT = game.level.tiles[0].size
                TICKS_PER_SECOND = game.properties.ticksPerSecond
                UNIT_FALL_SPEED = game.properties.unitFallSpeed
                UNIT_JUMP_SPEED = game.properties.unitJumpSpeed
                UNIT_JUMP_TIME = game.properties.unitJumpTime
                UNIT_JUMP_HEIGHT = UNIT_JUMP_SPEED * UNIT_JUMP_TIME
                UNIT_PAD_JUMP_SPEED = game.properties.jumpPadJumpSpeed
                UNIT_PAD_JUMP_TIME = game.properties.jumpPadJumpTime
                UNIT_PAD_JUMP_HEIGHT = UNIT_PAD_JUMP_SPEED * UNIT_PAD_JUMP_TIME
                COST_HOR = 1.0 / UNIT_MAX_HOR_SPEED       // the bigger the speed, the lesser the cost
                COST_JUMP_VER = 1.0 / UNIT_JUMP_SPEED          // speed: tiles / sec   cost: how much seconds to move 1 tile
                COST_JUMP_VER_PAD = 1.0 / UNIT_PAD_JUMP_SPEED
                COST_JUMP_DIAG = max(COST_HOR, COST_JUMP_VER)
                COST_JUMP_DIAG_PAD = max(COST_HOR, COST_JUMP_VER_PAD)
                COST_FALL_VER = 1.0 / UNIT_FALL_SPEED
                COST_FALL_DIAG = max(COST_HOR, COST_FALL_VER)
                // init persistent variables
                dangerMap = Array(MAP_WIDTH) { Array(MAP_HEIGHT) { 0 } }
                obstaclesMap = Array(MAP_WIDTH) { Array(MAP_HEIGHT) { Obstacle.NOTHING } }
                itemsMap = Array(MAP_WIDTH) { Array(MAP_HEIGHT) { 0f } }
                costMap = Array(2) { z ->
                    Array(MAP_WIDTH) { x ->
                        Array(MAP_HEIGHT) { y ->
                            CostPoint(x, y, z, Direction.NO_DIRECTION, z, Double.MAX_VALUE,
                                    canJump = false, canCancel = false, jumpHeight = 0.0, calculated = false)
                        }
                    }
                }
                FIRING_POSITIONS = game.level.tiles.mapIndexed { x, column ->
                    column.mapIndexed { y, tile ->
                        if ((tile == Tile.EMPTY || tile == Tile.PLATFORM || tile == Tile.LADDER) &&
                                isGround(x, y - 1) && isPassable(x, y + 1))
                            Point(x, y)
                        else null
                    }.filterNotNull()
                }.flatten()
                evaluateWeapons(game)
                inited = true
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // HELPERS

        /** If dealing max damage possible */
        private fun timeToKill(health: Int, weapon: Weapon?): Double {
            return if (weapon == null) Double.MAX_VALUE
            else {
                val bullets = ceil(health.toDouble() / (weapon.params.bullet.damage + (weapon.params.explosion?.damage
                        ?: 0)).toDouble()).toInt()
                (weapon.fireTimer ?: 0.0) + (bullets - 1) * weapon.params.fireRate +
                        if (weapon.magazine < bullets) ceil((bullets - weapon.magazine).toDouble() / weapon.params.magazineSize) * weapon.params.reloadTime else 0.0
            }
        }

        /** If I deal max damage and he does too */
        private fun canIKillHimBeforeHeKillsMe(enemy: GUnit): Boolean {
            return timeToKill(enemy.health, me.weapon) < timeToKill(me.health, enemy.weapon)
        }

        /** If everyone dies because of random rocket, would I win? */
        private fun inCaseOfDeathWillItBeProfitable(enemy: GUnit): Boolean {
            val iHaveRocketLauncher = me.weapon?.typ == WeaponType.ROCKET_LAUNCHER
            return (!iHaveRocketLauncher) ||
                    (me.health > game.properties.weaponParams[WeaponType.ROCKET_LAUNCHER]!!.explosion!!.damage) ||
                    run {
                        val myScore = game.players.find { it.id == MY_PLAYER_ID }!!.score
                        val enemyScore = game.players.find { it.id != MY_PLAYER_ID }!!.score
                        return (myScore + enemy.health + 1000 > enemyScore + 1000)
                    }
        }

        private fun isItSafeToRush(enemy: GUnit): Boolean {
            return canIKillHimBeforeHeKillsMe(enemy) && inCaseOfDeathWillItBeProfitable(enemy)
        }

        private fun inClinchZone(enemy: GUnit): Boolean {
            return abs(me.position.x - enemy.position.x) < 2 && abs(me.position.y - enemy.position.y) < 3
        }

        private fun isReloading(unit: GUnit): Boolean {
            return if (unit.weapon != null) {
                unit.weapon!!.magazine <= 1 &&
                        (unit.weapon!!.fireTimer ?: 0.0) > min(0.4, unit.weapon!!.params.fireRate)
            } else false
        }

        private fun canShoot(unit: GUnit): Boolean {
            return unit.weapon != null && (unit.weapon!!.fireTimer ?: 0.0) <= 0.2
        }

        private fun hasRocketLauncher(unit: GUnit?) = unit?.weapon?.typ == WeaponType.ROCKET_LAUNCHER

        private fun evaluateWeapons(game: Game) {
            game.properties.weaponParams.forEach { (type, params) ->
                preferredWeaponType[type] =
                        (params.magazineSize * params.bullet.damage + (params.explosion?.damage ?: 0)) /
                                (params.fireRate * (params.magazineSize - 1) + params.reloadTime)
            }
        }

        private fun shouldISwap(current: Weapon?, target: WeaponType?): Boolean {
            if (current == null) return true
            if (target == null) return false
            return preferredWeaponType[target]!! > preferredWeaponType[current.typ]!!
        }

        private fun itemCorrespondsType(item: Item, type: ItemType): Boolean {
            return type == ItemType.ANYTHING ||
                    (type == ItemType.WEAPON && item is Item.Weapon) ||
                    (type == ItemType.HEALTH && item is Item.HealthPack) ||
                    (type == ItemType.MINE && item is Item.Mine)
        }

        @Suppress("unused")
        private fun getItemType(item: Item): ItemType {
            return when (item) {
                is Item.Weapon -> ItemType.WEAPON
                is Item.HealthPack -> ItemType.HEALTH
                is Item.Mine -> ItemType.MINE
                else -> ItemType.ANYTHING
            }
        }

        private val nearestBlocks = arrayOf(
                Point(-1, 2), Point(0, 2), Point(1, 2),
                Point(-1, 1), Point(1, 1),
                Point(-1, 0), Point(1, 0),
                Point(-1, -1), Point(0, -1), Point(1, -1)
        )
        private val nearestBlocksPriority = mapOf(
                Direction.UP_LEFT to arrayOf(0, 1, 3, 2, 5, 4, 7, 6, 8, 9),
                Direction.UP_RIGHT to arrayOf(2, 1, 4, 0, 6, 3, 9, 5, 8, 7),
                Direction.UP to arrayOf(1, 0, 2, 3, 4, 5, 6, 7, 9, 8),
                Direction.LEFT to arrayOf(3, 5, 0, 7, 1, 8, 2, 9, 4, 6),
                Direction.RIGHT to arrayOf(4, 6, 2, 9, 1, 8, 0, 7, 3, 5),
                Direction.DOWN to arrayOf(8, 7, 9, 5, 6, 3, 4, 0, 2, 1),
                Direction.DOWN_LEFT to arrayOf(7, 5, 8, 3, 9, 0, 6, 1, 4, 2),
                Direction.DOWN_RIGHT to arrayOf(9, 8, 6, 7, 4, 5, 2, 3, 1, 0)
        )

        private fun nearestNotBlockedForPassageTile(x: Int, y: Int): Point? {
            if (!isBlockedForPassage(x, y)) return Point(x, y)
            if (!isBlockedForPassage(x, y + 1)) return Point(x, y + 1)

            val deltaX = x - me.position.x
            val deltaY = y - me.position.y
            val direction = if (deltaX > 0) {
                when {
                    deltaY > 0 -> Direction.DOWN_LEFT
                    deltaX == 0.0 -> Direction.LEFT
                    else -> Direction.UP_LEFT
                }
            } else if (deltaX == 0.0) {
                if (deltaY > 0) Direction.DOWN
                else Direction.UP
            } else {
                when {
                    deltaY > 0 -> Direction.DOWN_RIGHT
                    deltaX == 0.0 -> Direction.RIGHT
                    else -> Direction.UP_RIGHT
                }
            }
            nearestBlocksPriority[direction]?.forEach { index ->
                val point = nearestBlocks[index]
                if (!isBlockedForPassage(x + point.x, y + point.y)) return Point(x + point.x, y + point.y)
            }

            return null
        }


        private val RECT_SIZE = Vec2Float(0.5f, 0.5f)

        private fun renderRect(x: Int, y: Int, color: ColorFloat) {
            debug.draw(CustomData.Rect(Vec2Float(x.toFloat() + 0.25f, y.toFloat() + 0.25f), RECT_SIZE, color))
        }

        private fun renderLine(x1: Double, y1: Double, x2: Double, y2: Double, width: Float, color: ColorFloat) {
            debug.draw(CustomData.Line(Vec2Float(x1.toFloat(), y1.toFloat()), Vec2Float(x2.toFloat(), y2.toFloat()), width, color))
        }

        private fun renderText(x: Double, y: Double, text: String, color: ColorFloat, alignment: TextAlignment = TextAlignment.CENTER, size: Float = 12f) {
            debug.draw(CustomData.PlacedText(text, Vec2Float(x.toFloat(), y.toFloat()), alignment, size, color))
        }

        private fun renderCross(x: Double, y: Double, color: ColorFloat) {
            renderLine(x - 0.25, y - 0.25, x + 0.25, y + 0.25, 0.1f, color)
            renderLine(x - 0.25, y + 0.25, x + 0.25, y - 0.25, 0.1f, color)
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // MAPS

        private fun clearMaps() {
            for (x in 0 until MAP_WIDTH) {
                for (y in 0 until MAP_HEIGHT) {
                    dangerMap[x][y] = 0
                    obstaclesMap[x][y] = Obstacle.NOTHING
                    itemsMap[x][y] = 0f
                    costMap[0][x][y].calculated = false
                    costMap[1][x][y].calculated = false
                }
            }
        }

        // bounds control

        @Suppress("unused")
        private fun isInBounds(x: Double, y: Double): Boolean = x >= 0 && x < MAP_WIDTH && y >= 0 && y < MAP_HEIGHT

        private fun isInBounds(x: Int, y: Int): Boolean = x >= 0 && x < MAP_WIDTH && y >= 0 && y < MAP_HEIGHT

        private fun isWall(x: Int, y: Int): Boolean = isTile(x, y, Tile.WALL)
        private fun isTile(x: Int, y: Int, tile: Tile): Boolean {
            if (!isInBounds(x, y)) return true
            return game.level.tiles[x][y] == tile
        }

        // Returns true for out of bounds, walls, ladders and platforms
        private fun isGround(x: Int, y: Int): Boolean {
            if (!isInBounds(x, y)) return true
            val tile = game.level.tiles[x][y]
            return tile == Tile.WALL || tile == Tile.LADDER || tile == Tile.PLATFORM
        }

        // Returns false for out of bound, true for empty tiles, ladders and platforms
        private fun isPassable(x: Int, y: Int): Boolean {
            if (!isInBounds(x, y)) return false
            val tile = game.level.tiles[x][y]
            return tile == Tile.EMPTY || tile == Tile.LADDER || tile == Tile.PLATFORM
        }

        // danger calculation

        private fun increaseDanger(x: Int, y: Int, damage: Int) {
            if (isInBounds(x, y)) dangerMap[x][y] += damage
        }

        private fun increaseDanger(x: Double, y: Double, damage: Int) = increaseDanger(x.toInt(), y.toInt(), damage)

        @Suppress("unused")
        private fun getDanger(x: Double, y: Double): Int = dangerMap[x.toInt()][y.toInt()]

        private fun setExplosionDanger(x: Double, y: Double, radius: Double, damage: Int) {
            var ix = x - radius + 0.5
            var iy = y - radius + 0.5
            while (iy < y + radius + 0.5) {
                while (ix < x + radius + 0.5) {
                    increaseDanger(ix, iy, damage)
                    ix++
                }
                iy++
                ix = x - radius + 0.5
            }
        }

        private const val FORECAST_DEPTH = 6
        private const val EXPLOSIVE_FORECAST_DEPTH = 17

        private fun calculateBulletDangerForecast(bullet: Bullet) {
            val depth = if (bullet.explosionParams != null) EXPLOSIVE_FORECAST_DEPTH else FORECAST_DEPTH
            val targetX = bullet.position.x + bullet.velocity.x / TICKS_PER_SECOND * depth
            val targetY = bullet.position.y + bullet.velocity.y / TICKS_PER_SECOND * depth
            if (bullet.playerId != MY_PLAYER_ID) {
                val predicate: (Double, Double, Int, Int) -> Boolean = { _, _, xi, yi ->
                    if (isBlockedForBullets(xi, yi)) {
                        true
                    } else {
                        increaseDanger(xi, yi, bullet.damage)
                        false
                    }
                }
                val o = bullet.size / 2
                advancedTracer(bullet.position.x - o, bullet.position.y - o, targetX - o, targetY - o, predicate)
                advancedTracer(bullet.position.x + o, bullet.position.y - o, targetX + o, targetY - o, predicate)
                advancedTracer(bullet.position.x - o, bullet.position.y + o, targetX - o, targetY + o, predicate)
                advancedTracer(bullet.position.x + o, bullet.position.y + o, targetX + o, targetY + o, predicate)
            }
            if (bullet.explosionParams != null) {
                val hit = bulletTracerSqr(bullet.position.x, bullet.position.y, targetX, targetY, bullet.size)
                val criticalDistance = distanceSqr(bullet.position.x, bullet.position.y, targetX, targetY)
                if (hit != null && hit.distance <= criticalDistance) {
                    setExplosionDanger(hit.collisionX, hit.collisionY, bullet.explosionParams!!.radius, bullet.explosionParams!!.damage)
                    if (RENDER_DEBUG && RENDER_DEBUG_MAPS) renderCross(hit.collisionX, hit.collisionY, COLOR_RED)
                }
            }
        }

        private fun calculateDangerMap() {
            for (bullet in game.bullets) {
                calculateBulletDangerForecast(bullet)
            }
            for (mine in game.mines) {
                setExplosionDanger(mine.position.x, mine.position.y + mine.size.y / 2, mine.explosionParams.radius, mine.explosionParams.damage)
            }
        }

        // obstacles control

        private fun calculateObstaclesMap() {
            for (mine in game.mines) {
                val x = mine.position.x.toInt()
                val y = mine.position.y.toInt()
                obstaclesMap[x][y] = Obstacle.MINE
            }
            for (enemy in game.units) {
                val x = enemy.position.x.toInt()
                val y = enemy.position.y.toInt()
                if (enemy.playerId != MY_PLAYER_ID) {
                    obstaclesMap[x][y] = Obstacle.ENEMY
                    obstaclesMap[x][y + 1] = Obstacle.ENEMY
                    // draw phantom wall
                    val deltaY = abs(enemy.position.y - me.position.y)
                    if (deltaY < 2.0) {
                        if (enemy.jumpState.canJump) {
                            val remainingJumpTiles = ceil(enemy.jumpState.maxTime * enemy.jumpState.speed).toInt()
                            for (i in 0 until remainingJumpTiles) {
                                if (isBlockedForPassage(x, y + 2 + i) && obstaclesMap[x][y + 2 + i] != Obstacle.ME) break
                                obstaclesMap[x][y + 2 + i] = Obstacle.PHANTOM_WALL
                            }
                        }
                        if (enemy.jumpState.canCancel || !enemy.jumpState.canJump) {
                            for (i in 0 until MAP_HEIGHT) {
                                if (isBlockedForPassage(x, y - 1 - i) && obstaclesMap[x][y - 1 - i] != Obstacle.ME) break
                                obstaclesMap[x][y - 1 - i] = Obstacle.PHANTOM_WALL
                            }
                        }
                    }
                } else {
                    obstaclesMap[x][y] = Obstacle.ME
                    obstaclesMap[x][y + 1] = Obstacle.ME
                }
            }
        }

        private fun isBlockedForPassage(x: Int, y: Int): Boolean =
                isWall(x, y) || (obstaclesMap[x][y] != Obstacle.NOTHING && obstaclesMap[x][y] != Obstacle.ME)

        private fun isBlockedForBullets(x: Int, y: Int): Boolean =
                isWall(x, y) || (obstaclesMap[x][y] != Obstacle.NOTHING && obstaclesMap[x][y] != Obstacle.PHANTOM_WALL && obstaclesMap[x][y] != Obstacle.ME)

        // items control

        private fun calculateItemsMap() {
            for (box in game.lootBoxes) {
                val price = when {
                    itemCorrespondsType(box.item, ItemType.HEALTH) -> USEFUL_ITEM_HEALTH
                    itemCorrespondsType(box.item, ItemType.MINE) -> USEFUL_ITEM_MINE
                    else -> 0f
                }
                circularTracer(box.position.x, box.position.y + 0.5, USEFUL_ITEM_DIAMETER.toInt()) { x, y, diameter ->
                    val xi = x.toInt()
                    val yi = y.toInt()
                    if (isInBounds(xi, yi)) {
                        itemsMap[xi][yi] = itemsMap[xi][yi] + price * (USEFUL_ITEM_DIAMETER - diameter + 1) / USEFUL_ITEM_DIAMETER
                    }
                    false
                }
                itemsMap[box.position.x.toInt()][box.position.y.toInt()] = 0f
            }
        }

        // cost estimation

        private fun directionCost(direction: Direction, jumper: Boolean): Double {
            return when (direction) {
                Direction.LEFT, Direction.RIGHT -> COST_HOR
                Direction.DOWN -> if (jumper) COST_JUMP_VER_PAD else COST_JUMP_VER  // if you come from the bottom - need to check for jump pad usage
                Direction.UP -> COST_FALL_VER
                Direction.DOWN_LEFT, Direction.DOWN_RIGHT -> if (jumper) COST_JUMP_DIAG_PAD else COST_JUMP_DIAG
                Direction.UP_LEFT, Direction.UP_RIGHT -> COST_FALL_DIAG
                Direction.NO_DIRECTION -> 0.0
            }
        }

        private fun direction(fromX: Int, fromY: Int, toX: Int, toY: Int): Direction {
            return when {
                fromX < toX -> when {
                    fromY < toY -> Direction.UP_RIGHT
                    fromY == toY -> Direction.RIGHT
                    fromY > toY -> Direction.DOWN_RIGHT
                    else -> Direction.NO_DIRECTION
                }
                fromX == toX -> when {
                    fromY < toY -> Direction.UP
                    fromY > toY -> Direction.DOWN
                    else -> Direction.NO_DIRECTION
                }
                fromX > toX -> when {
                    fromY < toY -> Direction.UP_LEFT
                    fromY == toY -> Direction.LEFT
                    fromY > toY -> Direction.DOWN_LEFT
                    else -> Direction.NO_DIRECTION
                }
                else -> Direction.NO_DIRECTION
            }
        }

        // points currently under investigation (x, y, and the previous point in the chain)
        private val points = mutableListOf<Triple<Int, Int, CostPoint>>()

        private fun addPointToTheList(x: Int, y: Int, previousPoint: CostPoint) {
            for (i in 0 until points.size) {
                if (previousPoint.cost <= points[i].third.cost) {
                    points.add(i, Triple(x, y, previousPoint))
                    return
                }
            }
            points.add(Triple(x, y, previousPoint))
        }

        private fun calculatePoint(point: Triple<Int, Int, CostPoint>) {
            val x = point.first
            val y = point.second
            val prevPoint = point.third
            if (!isBlockedForPassage(x, y) && !isBlockedForPassage(x, y + 1)) { // check the room for head
                val onFloor = isGround(x, y - 1)
                val underRoof = isBlockedForPassage(x, y + 2)
                val onJumpPad = isTile(x, y, Tile.JUMP_PAD) || isTile(x, y - 1, Tile.JUMP_PAD)
                val currentDirection = direction(prevPoint.x, prevPoint.y, x, y)
                val comingDown = currentDirection == Direction.DOWN || currentDirection == Direction.DOWN_LEFT || currentDirection == Direction.DOWN_RIGHT
                val comingUp = currentDirection == Direction.UP || currentDirection == Direction.UP_LEFT || currentDirection == Direction.UP_RIGHT
                val comindFromTheSides = currentDirection == Direction.LEFT || currentDirection == Direction.RIGHT
                val cost = prevPoint.cost + directionCost(currentDirection, onJumpPad || prevPoint.canJump && !prevPoint.canCancel) + (dangerMap[x][y] + dangerMap[x][y + 1]) * 10
                val jumpHeight =
                        when {
                            onJumpPad -> UNIT_PAD_JUMP_HEIGHT
                            onFloor -> UNIT_JUMP_HEIGHT
                            comingUp -> prevPoint.jumpHeight - 1
                            comingDown -> 0.0
                            else -> prevPoint.jumpHeight
                        }
                val canJump =
                        if (onFloor || onJumpPad) true
                        else if (underRoof || comingDown || comindFromTheSides || jumpHeight < 1.0) false
                        else prevPoint.canJump

                val z = if (canJump) 0 else 1
                val currentPoint = costMap[z][x][y]

                if (!currentPoint.calculated || currentPoint.cost > cost) {
                    // set new path data
                    currentPoint.jumpHeight = jumpHeight
                    currentPoint.fromZ = prevPoint.z
                    currentPoint.direction = currentDirection
                    currentPoint.cost = cost
                    currentPoint.canJump =
                            if (onFloor || onJumpPad) true
                            else if (underRoof || comingDown || comindFromTheSides || currentPoint.jumpHeight < 1.0) false
                            else prevPoint.canJump
                    currentPoint.canCancel =
                            if (underRoof || comingDown || onJumpPad) false
                            else if (onFloor) true
                            else prevPoint.canCancel
                    currentPoint.canJump = canJump
                    currentPoint.calculated = true
                    val isFalling = !currentPoint.canJump && !currentPoint.canCancel

                    // add this point neighbors to the list
                    if (onFloor && !onJumpPad && !isBlockedForPassage(x - 1, y) && !isBlockedForPassage(x - 1, y + 1))
                        addPointToTheList(x - 1, y, currentPoint)
                    if (currentPoint.canJump && !isBlockedForPassage(x - 1, y + 1) && !isBlockedForPassage(x - 1, y + 2) && !isBlockedForPassage(x - 1, y))
                        addPointToTheList(x - 1, y + 1, currentPoint)
                    if ((currentPoint.canCancel || isFalling) && !isBlockedForPassage(x - 1, y - 1) && !isBlockedForPassage(x - 1, y))
                        addPointToTheList(x - 1, y - 1, currentPoint)
                    if (currentPoint.canJump && !isBlockedForPassage(x, y + 1) && !isBlockedForPassage(x, y + 2))
                        addPointToTheList(x, y + 1, currentPoint)
                    if ((currentPoint.canCancel || isFalling) && !isBlockedForPassage(x, y - 1))
                        addPointToTheList(x, y - 1, currentPoint)
                    if (onFloor && !onJumpPad && !isBlockedForPassage(x + 1, y) && !isBlockedForPassage(x + 1, y + 1))
                        addPointToTheList(x + 1, y, currentPoint)
                    if (currentPoint.canJump && !isBlockedForPassage(x + 1, y + 1) && !isBlockedForPassage(x + 1, y + 2) && !isBlockedForPassage(x + 1, y))
                        addPointToTheList(x + 1, y + 1, currentPoint)
                    if ((currentPoint.canCancel || isFalling) && !isBlockedForPassage(x + 1, y - 1) && !isBlockedForPassage(x + 1, y))
                        addPointToTheList(x + 1, y - 1, currentPoint)
                }
            }
            // remove this point from the list
            points.remove(point)
        }

        private fun calculateCostMap() {
            val x = me.position.x.toInt()
            val y = me.position.y.toInt()
            val canJump = me.jumpState.canJump
            val canCancel = me.jumpState.canCancel
            val z = if (canJump) 0 else 1
            val jumpHeight = me.jumpState.maxTime * me.jumpState.speed
            points.clear()
            // add first candidate with fake previous point
            addPointToTheList(x, y, CostPoint(x, y, z, Direction.NO_DIRECTION, z, 0.0, canJump, canCancel, jumpHeight, calculated = true))
            // then process this point and all the future child-points
            while (points.size > 0) {
                calculatePoint(points.first())
            }
        }

        private fun minimalCostPoint(vararg points: CostPoint) = points.filter { it.calculated }.minBy { it.cost }

        private fun calculatePath(toX: Int, toY: Int): PathResult {
            if (!isInBounds(toX, toY)) return PathResult(false, null)
            else {
                val path = mutableListOf<Point>()
                var x = toX
                var y = toY
                var point = minimalCostPoint(costMap[0][x][y], costMap[1][x][y]) ?: return PathResult(false, null)

                while (point.direction != Direction.NO_DIRECTION && point.calculated && isInBounds(x, y)) {
                    if (RENDER_DEBUG && RENDER_DEBUG_PATH) renderRect(x, y, COLOR_MAGENTA)
                    path.add(Point(x, y))
                    when (point.direction) {
                        Direction.DOWN_LEFT -> {
                            y++; x++
                        }
                        Direction.DOWN -> y++
                        Direction.DOWN_RIGHT -> {
                            y++; x--
                        }
                        Direction.RIGHT -> x--
                        Direction.UP_RIGHT -> {
                            y--; x--
                        }
                        Direction.UP -> y--
                        Direction.UP_LEFT -> {
                            y--; x++
                        }
                        Direction.LEFT -> x++
                        else -> {
                        }
                    }
                    point = costMap[point.fromZ][x][y]
                }
                return PathResult(distanceSqr(x, y, toX, toY) < 1.0, path)
            }
        }

        private fun minimalCost(a: CostPoint, b: CostPoint): Double {
            var cost = Double.MAX_VALUE
            if (a.calculated && a.cost < cost) cost = a.cost
            if (b.calculated && b.cost < cost) cost = b.cost
            return cost
        }

        private fun getMinimalCost(x: Int, y: Int): Double =
                if (isInBounds(x, y)) minimalCost(costMap[0][x][y], costMap[1][x][y])
                else Double.MAX_VALUE

        private fun getMinimalCost(x: Double, y: Double): Double = getMinimalCost(x.toInt(), y.toInt())

        private fun canGoTo(x: Int, y: Int) = getMinimalCost(x, y) < Double.MAX_VALUE
        private fun canGoTo(x: Double, y: Double) = canGoTo(x.toInt(), y.toInt())

        // render

        private fun renderMaps() {
            for (x in 0 until MAP_WIDTH) {
                for (y in 0 until MAP_HEIGHT) {
                    if (dangerMap[x][y] > 0) {
                        renderRect(x, y, ColorFloat(1f, 1f, 0f, dangerMap[x][y] / 100.0f))
                    }
                    if (itemsMap[x][y] > 0) {
                        val a = 0.1f + itemsMap[x][y] / USEFUL_ITEM_DIAMETER
                        renderRect(x, y, ColorFloat(0f, 1f, 1f, a))
                    }
                    if (obstaclesMap[x][y] != Obstacle.NOTHING) {
                        if (isBlockedForBullets(x, y)) {
                            renderRect(x, y, ColorFloat(1f, 0f, 0f, 0.5f))
                        } else if (isBlockedForPassage(x, y)) {
                            renderRect(x, y, ColorFloat(1f, 0.5f, 0f, 0.5f))
                        }
                    }
                    if (RENDER_DEBUG_MAPS_COST && costMap[0][x][y].calculated) {
                        val a = 0.1f + costMap[0][x][y].cost.toFloat() / 4.0f
                        renderText(x + 0.25, y + 0.75, costMap[0][x][y].cost.toString().take(4), ColorFloat(1f, 0f, 0f, a))
                    }
                    if (RENDER_DEBUG_MAPS_COST && costMap[1][x][y].calculated) {
                        val a = 0.1f + costMap[1][x][y].cost.toFloat() / 4.0f
                        renderText(x + 0.75, y + 0.25, costMap[1][x][y].cost.toString().take(4), ColorFloat(1f, 0f, 0f, a))
                    }
                }
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // PATHFINDER

        private fun nearestEnemy(me: GUnit, game: Game): GUnit? {
            var nearest: GUnit? = null
            for (other in game.units) {
                if (other.playerId != me.playerId) {
                    if (nearest == null || getMinimalCost(other.position.x, other.position.y) <
                            getMinimalCost(nearest.position.x, nearest.position.y)) {
                        nearest = other
                    }
                }
            }
            return nearest
        }

        private fun nearestItem(game: Game, type: ItemType): LootBox? {
            var nearest: LootBox? = null
            for (lootBox in game.lootBoxes) {
                if (itemCorrespondsType(lootBox.item, type)) {
                    if (nearest == null || getMinimalCost(lootBox.position.x, lootBox.position.y) <
                            getMinimalCost(nearest.position.x, nearest.position.y)) {
                        nearest = lootBox
                    }
                }
            }
            return nearest
        }

        private fun anyItem(game: Game, type: ItemType): LootBox? {
            for (lootBox in game.lootBoxes) {
                if (itemCorrespondsType(lootBox.item, type) && canGoTo(lootBox.position.x, lootBox.position.y)) {
                    return lootBox
                }
            }
            return null
        }

        /** In case of hiding the bullet must be of the enemy, in case of fighting - yours */
        private fun nearestFiringPoint(enemyX: Double, enemyY: Double, hide: Boolean, bulletSize: Double, minDistToEnemy: Double = 0.0): Point? {
            if (FIRING_POSITIONS.isNotEmpty()) {
                var nearestPoint: Point? = null
                var nearestBlackBoxRating = Double.MAX_VALUE

                val minDistSqr = minDistToEnemy.pow(2)

                for (point in FIRING_POSITIONS) {
                    val aimDistSqr = distanceSqr(point.x.toDouble(), point.y.toDouble(), enemyX, enemyY)
                    if (aimDistSqr >= minDistSqr) {
                        val danger = dangerMap[point.x][point.y] + dangerMap[point.x][point.y + 1] * 1000 // lesser - better
                        val item = itemsMap[point.x][point.y]                                             // bigger - better
                        val path = getMinimalCost(point.x, point.y)                                              // lesser - better
                        val hit = bulletTracerSqr(point.x.toDouble() + 0.5, point.y.toDouble() + UNIT_HEIGHT / 2, enemyX, enemyY, bulletSize)
                        val hitRating = hit == null || !isInBounds(hit.tileX, hit.tileY) || obstaclesMap[hit.tileX][hit.tileY] == Obstacle.ENEMY

                        val blackBoxRating = danger + path - item + if (hitRating == !hide) 1.0 else -1.0

                        if (nearestPoint == null || blackBoxRating < nearestBlackBoxRating) {
                            if (RENDER_DEBUG && RENDER_DEBUG_PATH) renderText(point.x.toDouble() + 0.5, point.y.toDouble() + 0.6, blackBoxRating.toInt().toString(), COLOR_ORANGE)
                            nearestPoint = point
                            nearestBlackBoxRating = blackBoxRating
                        }
                    }
                }
                return nearestPoint
            } else return null
        }

        private fun renderFiringPositions() {
            for (point in FIRING_POSITIONS) {
                val x = point.x.toDouble()
                val y = point.y.toDouble()
                renderLine(x, y, x + 1, y + 1, 0.06f, COLOR_GRAY)
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // MATH

        private fun minimalPositive(vararg values: Double): Double {
            var min = Double.MAX_VALUE
            values.forEach { i -> if (i > 0 && i < min) min = i }
            return min
        }

        /**
         * Returns the next integer number
         */
        private fun plusCeil(value: Double): Double = if (value == ceil(value)) value + 1 else ceil(value)

        /**
         * Returns the previous integer number
         */
        private fun minusFloor(value: Double): Double = if (value == floor(value)) value - 1 else floor(value)

        private fun distanceSqr(x1: Double, y1: Double, x2: Double, y2: Double): Double {
            return (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)
        }

        private fun distanceSqr(x1: Int, y1: Int, x2: Int, y2: Int): Int {
            return (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)
        }

        @Suppress("unused")
        private fun len(vec: Vec2Double): Double {
            return sqrt(vec.x * vec.x + vec.y * vec.y)
        }

        @Suppress("unused")
        private fun angleDelta(a: Double, b: Double): Double = ((b + PI * 2) - (a + PI * 2)) % (PI * 2)

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // TRACERS

        private fun advancedTracer(fromX: Double, fromY: Double, toX: Double, toY: Double, predicate: (Double, Double, Int, Int) -> Boolean) {
            var x = fromX
            var y = fromY
            var xi = x.toInt()
            var yi = y.toInt()
            val txi = toX.toInt()
            val tyi = toY.toInt()
            val dx = toX - fromX
            val dy = toY - fromY
            do {
                if (predicate(x, y, xi, yi)) return
                val up = (plusCeil(y) - y) / dy
                val bottom = (minusFloor(y) - y) / dy
                val left = (minusFloor(x) - x) / dx
                val right = (plusCeil(x) - x) / dx
                val min = minimalPositive(up, bottom, left, right)
                x += dx * min
                y += dy * min
                when (min) {
                    up -> yi++
                    bottom -> yi--
                    left -> xi--
                    right -> xi++
                }
            } while (xi != txi || yi != tyi)
            predicate(x, y, xi, yi)
        }


        private fun simpleTracerSqr(fromX: Double, fromY: Double, toX: Double, toY: Double): Hit? {
            var hit: Hit? = null
            advancedTracer(fromX, fromY, toX, toY) { x, y, xi, yi ->
                if (isBlockedForBullets(xi, yi)) {
                    hit = Hit(distanceSqr(fromX, fromY, x, y), x, y, xi, yi)
                    true
                } else {
                    false
                }
            }
            return hit
        }

        private fun nearest(vararg hits: Hit?): Hit? = hits.filterNotNull().minBy { it.distance }

        private fun bulletTracerSqr(fromX: Double, fromY: Double, toX: Double, toY: Double, bulletSize: Double): Hit? {
            val o = bulletSize / 2
            return nearest(
                    simpleTracerSqr(fromX - o, fromY - o, toX - o, toY - o),
                    simpleTracerSqr(fromX - o, fromY + o, toX - o, toY + o),
                    simpleTracerSqr(fromX + o, fromY - o, toX + o, toY - o),
                    simpleTracerSqr(fromX + o, fromY + o, toX + o, toY + o))
        }

        private fun circularTracer(x: Double, y: Double, diameter: Int = CIRCULAR_SEARCH_DIAMETER, predicate: (Double, Double, Int) -> Boolean) {
            var ix = x
            var iy = y
            var i = 0
            var len = 0
            var step = 1
            var dir = 1 // 0 - right, 1 - down, 2 - left, 3 - up
            while (len <= diameter) {
                if (predicate(ix, iy, len)) return
                when (dir) {
                    0 -> ix++
                    1 -> iy--
                    2 -> ix--
                    3 -> iy++
                }
                i++
                if (i >= len) {
                    i = 0
                    dir = (dir + 1) % 4
                    step = (step + 1) % 2
                    if (step == 0) len++
                }
            }
        }
    }
}
